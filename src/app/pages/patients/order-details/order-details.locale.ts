export const OrderDetailsLocale = {
  medicationOrder: $localize`Medication order`,
  bloodGlucoseOrder: $localize`Self-monitoring blood glucose order`,
  timeLocale: $localize`en-gb`,
  duration: $localize`Duration`,
  frequency: $localize`Frequency`,
  daysOfWeek: $localize`Days of week`,
  timeOfDay: $localize`Times of day`,
  when: $localize`When`,
}
